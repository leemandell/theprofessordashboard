import React, { Component } from 'react';
import './App.css';
// let ResponsiveReactGridLayout = require('react-grid-layout').Responsive;
import {Grid, Row, Col, Panel} from 'react-bootstrap'
import xhr from 'xhr';
import Plot from './plot';

class App extends Component {


    state = {
        location: '',
        data: {},
        dates: [],
        temps: []
    };


    render() {
        let currentTemp = 28;
        console.log("In render");
        console.log("X data: " + this.state.dates);
        console.log("y data: " + this.state.temps);

        let hostNames = ["piruNorthGR3a", "piruNorthGR3b", "piruNorthGR3c"]

        return (
            <Grid  className = "container">
                <Row className = "show-grid">
                    <Col key={"1"} xs={12} sm={12} md={12}>
                        <Panel header = "PiruNorthGR3a" bsStyle = "primary">
                            <Plot
                                type="scatter"
                                hostname={["piruNorthGR3a", "piruNorthGR3a"]}
                                graphfield={["air_temp_f_dht", "air_humidity"]}
                                currentConditionsField="dhtTemperature_f"
                                title="Temperature"
                            />
                        </Panel>
                    </Col>
                 </Row>
            </Grid>


         )
    }
}

export default App;
