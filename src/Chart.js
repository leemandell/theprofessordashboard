import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';
var moment = require('moment');

function getJSONObject() {
    // "use strict";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/environment/piruNorthGR3a', false);
    // xhr.onload = function() {
    //     if (xhr.status === 200) {
    //         var jsonResponse = JSON.parse(xhr.responseText);
    //         processJSON(jsonResponse);
    //         console.log('User\'s name is ' + jsonResponse[0].hostname);
    //         return (jsonResponse);
    //     }
    //     else {
    //         console.log('Request failed.  Returned status of ' + xhr.status);
    //         return null;
    //     }
    // };
    xhr.send();
    if (xhr.status === 200) {
        var jsonResponse = JSON.parse(xhr.responseText);
        processJSON(jsonResponse);
        console.log('User\'s name is ' + jsonResponse[0].hostname);
        return (jsonResponse);
    }
    else {
        console.log('Request failed.  Returned status of ' + xhr.status);
        return null;
    }
}


let labelArray = [];
let dataArray = [];

function processJSON(jsonObjects) {
    console.log("length: " + jsonObjects.length);
    var i;
    var date ;

    for (i = 0; i < jsonObjects.length; i++) {
        // console.log("Hostname: " + jsonObjects[i].date);
        // date = new Date(jsonObjects[i].date)
        // labelArray.push(moment(date).format( 'MMM DD'));
        labelArray.push(new Date(jsonObjects[i].date).toJSON());
        dataArray.push(jsonObjects[i].air_temp_f_dht);
    //     if (i > 100) {
    //         break;
    //     }
    }
}

var data = {

    labels: labelArray,
    title: {
        text: "Date Time Formatting"
    },

    datasets: [
        {
            label: 'Temperature',
            data: dataArray,
            tension: 0,
            borderColor: "rgb(248,169,113)",
            backgroundColor: "rgba(0,0,0,0)",
            radius: 0,
            borderWidth: 1,
            pointHitRadius: 5

        }
    ]


};

var options = {

    responsive: true,
    title:{
        display:true,
        text:"Chart.js Time Point Data"
    },
    scales: {
        xAxes: [{
            type: "time",
            time: {
                format: "MMM DD HH:mm",
                // unit: 'minute',
                // unitStepSize: 24*60,
                displayFormats: {
                    'minute': 'MMM DD HH:mm',
                    'hour': 'MMM DD HH:mm',
                },
                tooltipFormat: 'HH:mm'
            }
        }]
    }
};

let currentCondition;

function getCurrentCondition() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/currentConditions/piruWestRoom1',false);
    xhr.onload = function() {
        if (xhr.status === 200) {
            var jsonResponse = JSON.parse(xhr.responseText);
            currentCondition = jsonResponse[0].dhtTemperature_f;
        }
        else {
            console.log('Request failed.  Returned status of ' + xhr.status);
            return null;
        }
    };
    xhr.send();
        // if (xhr.status === 200) {
        //     var jsonResponse = JSON.parse(xhr.responseText);
        //     currentCondition = jsonResponse[0].dhtTemperature_f;
        //     console.log("Current conditions: " + jsonResponse);
        // }
        // else {
        //     console.log('Request failed.  Returned status of ' + xhr.status);
        //     return null;
        // }

}

class Chart extends(Component) {

    componentWillMount() {
         let json = getJSONObject();
         getCurrentCondition();
    }

    render() {
        // console.log ("Labels: " + labelArray);
        return (

            <div>
                <h2>Temperature</h2>
                <h1>{currentCondition}</h1>
                 <Line data={data} options={options}/>
            </div>
        );
    }

};

export default Chart;

// export default React.createClass({
//     displayName: 'LineExample',
//
//     render() {
//         return (
//             <div>
//                 <h2>Line Example</h2>
//                 <Line data={data} />
//             </div>
//         );
//     }
// });