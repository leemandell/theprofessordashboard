import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';
var moment = require('moment');



function newDate(days) {
    return moment().add(days, 'h').toDate();
}
function newDateString(days) {
    return moment().add(days, 'h').format();
}

var data = {

       datasets: [{
            label: "Dataset with string point data",
            backgroundColor: "rgba(248,169,113, .3)",
            borderColor: "rgba(248,169,113, 1)",
            fill: false,
            data: [{
                x: newDate(0),
                y: 5
            }, {
                x: newDate(2),
                y: 10
            }, {
                x: newDate(4),
                y: 3
            }, {
                x: newDate(5),
                y: 8
            }],
        }, {
            label: "Dataset with date object point data",
            backgroundColor: "rgba(150,50,113,.3)",
            borderColor: "rgba(150,50,113,1)",
            fill: false,
            data: [{
                x: newDate(0),
                y: 7
            }, {
                x: newDate(2),
                y: 2
            }, {
                x: newDate(4),
                y: 10
            }, {
                x: newDate(5),
                y: 3
            }]
        }],

        displayFormats: {
            millisecond: 'MMM DDtt',
            second: 'MMM DDtt',
            minute: 'MMM DDtt',
            hour: 'MMM DDtt',
            day: 'MMM DDtt',
            week: 'MMM DDtt',
            month: 'MMM DDtt',
            quarter: 'MMM DDtt',
            year: 'MMM DDtt',
        }



};


var options = {

        responsive: true,
        title:{
            display:true,
            text:"Chart.js Time Point Data"
        },
    scales: {
        xAxes: [{
            type: "time",
            time: {
                format: "HH:mm",
                unit: 'hour',
                unitStepSize: 2,
                displayFormats: {
                    'minute': 'HH:mm',
                    'hour': 'HH:mm',
                },
                tooltipFormat: 'HH:mm'
            }
        }],
        yAxes: [{
            position: "left"
        }]
    }
};


class Chart2 extends(Component) {

    render() {
        // console.log ("Labels: " + labelArray);
        return (

            <div>
                <h2>testing</h2>
                <Line data={data} options={options}/>
            </div>
        );
    }

};

export default Chart2;

