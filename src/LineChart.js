import React from 'react';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, ReferenceLine,
    ReferenceDot, Tooltip, CartesianGrid, Legend, Brush, ErrorBar, AreaChart, Area } from 'recharts';
import { scalePow, scaleLog } from 'd3-scale';
var moment = require('moment');

function getJSONObject() {
    // "use strict";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/environment/piruNorthGR3a', false);
    // xhr.onload = function() {
    //     if (xhr.status === 200) {
    //         var jsonResponse = JSON.parse(xhr.responseText);
    //         processJSON(jsonResponse);
    //         console.log('User\'s name is ' + jsonResponse[0].hostname);
    //         return (jsonResponse);
    //     }
    //     else {
    //         console.log('Request failed.  Returned status of ' + xhr.status);
    //         return null;
    //     }
    // };
    xhr.send();
    if (xhr.status === 200) {
        var jsonResponse = JSON.parse(xhr.responseText);
        processJSON(jsonResponse);
        console.log('User\'s name is ' + jsonResponse[0].hostname);
        return (jsonResponse);
    }
    else {
        console.log('Request failed.  Returned status of ' + xhr.status);
        return null;
    }
}
const dateFormat = (time) => {
    return moment(time).format('M/D ha');
};

const labelFormat = (time) => {
    return moment(time).format('M/D h:m a');
};


let labelArray = [];
let dataArray = [];

function processJSON(jsonObjects) {
    console.log("length: " + jsonObjects.length);
    var i;
    var date ;
    var dateString;

    for (i = jsonObjects.length-1; i > 0 ; i--) {
        // console.log("Hostname: " + jsonObjects[i].date);
        // date = new Date(jsonObjects[i].date);
        // dateString = moment(date).format( 'mm/dd YYYY');
        // labelArray.push(moment(date).format( 'MMM DD'));
        labelArray.push(new Date(jsonObjects[i].date).toJSON());
        dataArray.push({date: new Date(jsonObjects[i].date).toJSON(),
                        temp: jsonObjects[i].air_temp_f_dht});
            // if (i > 100) {
            //     break;
            // }
    }
    // console.log("Data array: " + JSON.stringify(dataArray));
}


let currentCondition;

function getCurrentCondition() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/currentConditions/piruWestRoom1',false);
    xhr.onload = function() {
        if (xhr.status === 200) {
            var jsonResponse = JSON.parse(xhr.responseText);
            currentCondition = jsonResponse[0].dhtTemperature_f;
        }
        else {
            console.log('Request failed.  Returned status of ' + xhr.status);
            return null;
        }
    };
    xhr.send();
    // if (xhr.status === 200) {
    //     var jsonResponse = JSON.parse(xhr.responseText);
    //     currentCondition = jsonResponse[0].dhtTemperature_f;
    //     console.log("Current conditions: " + jsonResponse);
    // }
    // else {
    //     console.log('Request failed.  Returned status of ' + xhr.status);
    //     return null;
    // }

}
getJSONObject();


export default React.createClass({
    displayName: 'LineChartDemo',

    getInitialState() {
        return null;
    },

    handleChangeData() {
        // this.setState(() => _.mapValues(initilaState, changeNumberOfData));
    },

    handleClick(data, e) {
        console.log(data);
    },

    handleLegendMouseEnter() {
        this.setState({
            opacity: 0.5,
        });
    },

    handleLegendMouseLeave() {
        this.setState({
            opacity: 1,
        });
    },

    handleChangeAnotherState() {
        this.setState({
            anotherState: !this.state.anotherState,
        })
    },

    render() {
        var tempMin = 65
        getCurrentCondition();

        return (
                <div  className="text-center">
                    <h2  className="text-center">Temperature</h2>
                    <h1   className="text-center">{currentCondition}</h1>
                    <LineChart
                        align="center"
                        width={300} height={300}
                        data={dataArray}
                    >
                        <CartesianGrid vertical={false} />
                        <XAxis dataKey="date"  tickFormatter={dateFormat}/>
                        <YAxis domain={[tempMin, 85]} />
                        <Tooltip  labelFormatter={labelFormat}/>
                        <Line dataKey="temp" stroke="#ff7300" dot={false} />
                   </LineChart>
                </div>
        );
    }
});