/* global Plotly */
// Plot.js
import React from 'react';
let moment = require('moment');
import newId from './utils/newid';

let currentCondition = "";




class Plot extends React.Component {



    labelArray = [];
    dataArray = [];
    plotID = "test";

    constructor(props) {
        super(props);
        this.updatePlotSize = this.updatePlotSize.bind(this);
    }

    getCurrentCondition(hostname) {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/currentConditions/' + hostname,false);
        let currentConditionsField = this.props.currentConditionsField;
        xhr.onload = function() {
            if (xhr.status === 200) {
                let jsonResponse = JSON.parse(xhr.responseText);
                currentCondition = jsonResponse[0][currentConditionsField];
            }
            else {
                console.log('Request failed.  Returned status of ' + xhr.status);
                return null;
            }
        };
        xhr.send();
        // if (xhr.status === 200) {
        //     var jsonResponse = JSON.parse(xhr.responseText);
        //     currentCondition = jsonResponse[0].dhtTemperature_f;
        //     console.log("Current conditions: " + jsonResponse);
        // }
        // else {
        //     console.log('Request failed.  Returned status of ' + xhr.status);
        //     return null;
        // }

    }


    getJSONObject(hostname, graphField) {
        // "use strict";
        console.log("Hostname: " + hostname);
        console.log("Graph field: " + graphField);
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://api-quadroponic.rhcloud.com/v1/report/environment/' +hostname, false);
        xhr.send();
        if (xhr.status === 200) {
            let jsonResponse = JSON.parse(xhr.responseText);
            // console.log("JSON Response: " + JSON.stringify(jsonResponse));
            this.processJSON(jsonResponse, graphField);
            console.log('User\'s name is ' + jsonResponse[0].hostname);
            return (jsonResponse);
        }
        else {
            console.log('Request failed.  Returned status of ' + xhr.status);
            return null;
        }
    }

    processJSON(jsonObjects, field) {
        console.log("jsonObjects field: " + field);
        console.log("length: " + jsonObjects.length);
        let i;

        for (i = jsonObjects.length-1; i > 0 ; i--) {
           // console.log("Hostname: " + jsonObjects[i].date);
            // date = new Date(jsonObjects[i].date);
            // dateString = moment(date).format( 'mm/dd YYYY');
            // labelArray.push(moment(date).format( 'MMM DD'));
            this.labelArray.push(new Date(jsonObjects[i].date));
            this.dataArray.push(jsonObjects[i][field]);
               // if (i > 5) {
               //      break;
               //  }
        }
        // console.log("Data array: " + JSON.stringify(dataArray));
    }


    plot() {
        console.log("Hostnames: " + this.props.hostname);
        console.log("Graph fields: " + this.props.graphfield);
        this.getJSONObject( this.props.hostname[0],this.props.graphfield[0]);
        this.getCurrentCondition( this.props.hostname[0]);
        let data = [];
        let trace = {
            x: this.labelArray,
            y: this.dataArray,
            type: 'scatter',
            name:this.props.graphfield[0],
            line: {
                color: 'rgb(219, 64, 82)',
                width:.5
            }


        };
        data.push(trace);

        let yaxisLabel;

        for (let i =1; i < this.props.hostname.length; i++) {
            console.log("Add trace for: " + this.props.hostname[i]);
            this.dataArray = [];
            this.labelArray = [];
            this.getJSONObject(this.props.hostname[i], this.props.graphfield[i]);
            yaxisLabel = "y" + (i+1);
            console.log ("yaxislabel: " + yaxisLabel);
            trace = {
                x:this.labelArray,
                y:this.dataArray,
                yaxis: yaxisLabel,
                name:this.props.graphfield[i],
                line: {
                    width:.5
                }
            };
            data.push(trace);
        }


            let layout = {
            title: "this is a title",
            wide_margin: { // update the left, bottom, right, top margin
                l: 160,
                b: 100,
                r: 160,
                t: 20
            },
            margin: { // update the left, bottom, right, top margin
                l: 50,
                b: 100,
                r: 50,
                t: 75
            },
                legend: {"orientation": "h",
                x:0,
                y:50},
            xaxis: {
                showgrid: false,
                tickangle: -45,
                _tickformat: "%a %I:%M%p %e-%b",
                tickformat: "%e-%b \n%I:%M%p",
                categoryorder: "category ascending",
                title: "date"
            },
            yaxis2: {
                title: 'Humidity',
                titlefont: {color: 'rgb(148, 103, 189)'},
                tickfont: {color: 'rgb(148, 103, 189)'},
                overlaying: 'y',
                side: 'right'
            }
        };
        layout["yaxis"] = {title: 'Temperature'};
        Plotly.newPlot(this.state.id, data, layout, {
            displayModeBar: true
        });
     }

    updatePlotSize() {
        console.log("resize event fired for id: " + this.plotID);
        let element = this.refs[this.state.id]
        Plotly.Plots.resize(element);
    }

    componentDidMount() {
        this.plot();
     }

     componentWillMount() {
        const id = newId();
        this.setState({id:id});
        this.plotID = id;
         window.addEventListener("resize", this.updatePlotSize)
     }

     componentWillUnmount() {
         window.removeEventListener("resize", this.updatePlotSize);
     }

    // componentWillUpdate() {
    //     this.plot();

        // console.log("In Plot X data: " + JSON.stringify(dataArray))
        // console.log("In Plot y data: " + labelArray)

    // }

    render() {
        console.log("Plot name: " + this.state.id);
        return (
            <div>
                <h3  className="text-center">{this.props.title}</h3>
                <h1   className="text-center">{currentCondition}</h1>
                <div id={this.state.id} ref = {this.state.id}>
                </div>
            </div>
        );
    }
}

export default Plot;